From: Martyn Welch <martyn.welch@collabora.com>
Date: Wed, 12 Jun 2024 17:20:01 +0100
Subject: ls1028ardb: Set fdtfile so that correct DTB is used

The environment variable `fdtfile` is currently not being set in the
environment. As a result, a device tree name is being constructed from
the `soc` and `board` environment variables, which doesn't match the
naming used for the actual device trees provided by the kernel. Use
`CONFIG_DEFAULT_FDT_FILE` to provide the required device tree name.

Signed-off-by: Martyn Welch <martyn.welch@collabora.com>
---
 configs/ls1028ardb_tfa_defconfig | 1 +
 include/configs/ls1028ardb.h     | 1 +
 2 files changed, 2 insertions(+)

diff --git a/configs/ls1028ardb_tfa_defconfig b/configs/ls1028ardb_tfa_defconfig
index de893ff..69909b9 100644
--- a/configs/ls1028ardb_tfa_defconfig
+++ b/configs/ls1028ardb_tfa_defconfig
@@ -29,6 +29,7 @@ CONFIG_FIT_VERBOSE=y
 CONFIG_DISTRO_DEFAULTS=y
 CONFIG_BOOTDELAY=10
 CONFIG_OF_BOARD_SETUP=y
+CONFIG_DEFAULT_FDT_FILE="fsl-ls1028a-rdb"
 CONFIG_OF_STDOUT_VIA_ALIAS=y
 CONFIG_USE_BOOTARGS=y
 CONFIG_BOOTARGS="console=ttyS0,115200 root=/dev/ram0 earlycon=uart8250,mmio,0x21c0500 ramdisk_size=0x2000000 default_hugepagesz=2m hugepagesz=2m hugepages=256 video=1920x1080-32@60 cma=256M"
diff --git a/include/configs/ls1028ardb.h b/include/configs/ls1028ardb.h
index ee4f885..692d06f 100644
--- a/include/configs/ls1028ardb.h
+++ b/include/configs/ls1028ardb.h
@@ -59,6 +59,7 @@
 #undef CFG_EXTRA_ENV_SETTINGS
 #define CFG_EXTRA_ENV_SETTINGS		\
 	"board=ls1028ardb\0"			\
+	"fdtfile=freescale/" CONFIG_DEFAULT_FDT_FILE ".dtb\0" \
 	"hwconfig=fsl_ddr:bank_intlv=auto\0"	\
 	"ramdisk_addr=0x800000\0"		\
 	"ramdisk_size=0x2000000\0"		\
