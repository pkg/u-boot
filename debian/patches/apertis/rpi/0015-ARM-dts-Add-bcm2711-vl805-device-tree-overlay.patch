From: Ariel D'Alessandro <ariel.dalessandro@collabora.com>
Date: Thu, 13 Oct 2022 15:31:47 -0300
Subject: ARM: dts: Add bcm2711-vl805 device tree overlay

The CANOPi board comes with a VIA VL805 4 Port USB controller on the
PCIe bus, which requires an initialization sequence (e.g. firmware
loaded) to be run during boot.

In order to properly initialize this device, it needs to be declared in
the device tree. Otherwise, the PCIe bus may communicate with USB
controller during initialization, which hangs the board on a Synchronous
Abort, e.g.:

```
U-Boot menu
1:	Apertis v2023dev3 5.18.0-0.deb11.4-arm64
2:	Apertis v2023dev3 5.18.0-0.deb11.4-arm64 (rescue target)
Enter choice: 1:	Apertis v2023dev3 5.18.0-0.deb11.4-arm64
Retrieving file: /initrd.img-5.18.0-0.deb11.4-arm64
"Synchronous Abort" handler, esr 0x96000004
elr: 000000000009cc90 lr : 00000000000a8e50 (reloc)
elr: 000000003b370c90 lr : 000000003b37ce50
x0 : d519b040aa010000 x1 : 0000000000000058
x2 : 0000000000400000 x3 : 000000003b3d29c0
x4 : b900080152b00000 x5 : 000000003b3d2450
x6 : 000000000000005b x7 : 000000003b3d29f0
x8 : b900080152affdf0 x9 : 0000000000000008
x10: fffffffffffffff0 x11: 0000000000000006
x12: 000000003af58730 x13: 000000000001ea57
x14: 0000000000000000 x15: 0000000000000000
x16: 000000003b37fac4 x17: 656e2f74656e7265
x18: 000000003af4fd90 x19: 0000000000000210
x20: 000000003b3d2440 x21: 0000000000000001
x22: 0000000000000200 x23: 000000003af43e80
x24: 0000000000000200 x25: 000000000001ea56
x26: 0000000000000000 x27: 0000000000000006
x28: 0000000000000009 x29: 000000003af43d60

Code: eb02007f 54ffff48 f9400cc4 17ffffe0 (f9400404)
Resetting CPU ...
```

The RPi loader loads dtbs and overlays for u-boot from the firmware
partition. In the CANOPi case, it will try to load the vl805.dtbo
overlay, so let's provide it.

Signed-off-by: Ariel D'Alessandro <ariel.dalessandro@collabora.com>
---
 arch/arm/dts/Makefile            |  3 ++-
 arch/arm/dts/bcm2711-rpi-cm4.dts | 11 +++++++++++
 arch/arm/dts/bcm2711-vl805.dts   | 18 ++++++++++++++++++
 3 files changed, 31 insertions(+), 1 deletion(-)
 create mode 100644 arch/arm/dts/bcm2711-vl805.dts

diff --git a/arch/arm/dts/Makefile b/arch/arm/dts/Makefile
index 47061da..387a4b9 100644
--- a/arch/arm/dts/Makefile
+++ b/arch/arm/dts/Makefile
@@ -1312,7 +1312,8 @@ dtb-$(CONFIG_ARCH_BCM283X) += \
 	bcm2837-rpi-3-b-plus.dtb \
 	bcm2837-rpi-cm3-io3.dtb \
 	bcm2711-rpi-4-b.dtb \
-	bcm2711-rpi-cm4.dtb
+	bcm2711-rpi-cm4.dtb \
+	bcm2711-vl805.dtbo
 
 dtb-$(CONFIG_TARGET_BCMNS) += ns-board.dtb
 
diff --git a/arch/arm/dts/bcm2711-rpi-cm4.dts b/arch/arm/dts/bcm2711-rpi-cm4.dts
index 72ac95a..65ba4a2 100644
--- a/arch/arm/dts/bcm2711-rpi-cm4.dts
+++ b/arch/arm/dts/bcm2711-rpi-cm4.dts
@@ -93,6 +93,17 @@
 	};
 };
 
+&pcie0 {
+	pci@0,0 {
+		device_type = "pci";
+		#address-cells = <3>;
+		#size-cells = <2>;
+		ranges;
+
+		reg = <0 0 0 0 0>;
+	};
+};
+
 /* uart0 communicates with the BT module */
 &uart0 {
 	pinctrl-names = "default";
diff --git a/arch/arm/dts/bcm2711-vl805.dts b/arch/arm/dts/bcm2711-vl805.dts
new file mode 100644
index 0000000..81adf34
--- /dev/null
+++ b/arch/arm/dts/bcm2711-vl805.dts
@@ -0,0 +1,18 @@
+/dts-v1/;
+/plugin/;
+
+#include <dt-bindings/reset/raspberrypi,firmware-reset.h>
+
+/ {
+	compatible = "brcm,bcm2711";
+
+	fragment@0 {
+		target-path = "pcie0/pci@0,0";
+		__overlay__ {
+			usb@0,0 {
+				reg = <0 0 0 0 0>;
+				resets = <&reset RASPBERRYPI_FIRMWARE_RESET_ID_USB>;
+			};
+		};
+	};
+};
